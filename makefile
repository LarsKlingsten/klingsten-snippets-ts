test:
	tsc && node dist/test/runtests.js 

publish:
	npm login
	npm version patch && tsc && npm publish 
