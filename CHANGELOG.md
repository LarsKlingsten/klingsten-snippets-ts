# Versions

# 1.0.30
Replacing String to Number

## 1.0.29
bugfix. uploadFiles was neither static or public, as it needed to be

## 1.0.27
- adds Files class.
- @Files -> public static async uploadFiles(filesInfo: File[]): Promise<IUploadFile[]> 
- @Files -> public static async readFileAsync(file: File): Promise<string>
- interface  IUploadFile {file:File, data: string // Base64 url }

## 1.0.26
adds public static isEmailValid(s: string): boolean {

## 1.0.25
bugFix @splitNameFromEmail() when name and emails were both emails addresses, {email} would container two emails addresses, now its return one email address, and name is empty

## 1.0.25
bugFix @splitNameFromEmail() when name and emails were both emails addresses, {email} would container two emails addresses, now its return one email address, and name is empty

## 1.0.24
Compare get CompareError[] return type (instead of string[])

## 1.0.21
adds safeguards on Compare.objects, Compare.arrays, so arrays fails when passed to objects, and vice-versa

## 1.0.19
adds Strings.splitNameFromEmail, Strings.replaceIllgalCharsEmail

## 1.0.18
Compare.objects, Compare.arrays can now compare empty attributes  [''] 

## 1.0.15
adds  Compare.arrays(), Compare.objects(), Compare.printErrors()

## 0.0.14
adds Strings.splitByCommaSemicolon

## 0.0.13
adds Strings.removeNonPrintingCharsAndSpaces