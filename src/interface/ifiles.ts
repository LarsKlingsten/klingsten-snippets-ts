export interface IUploadFile {
    /** javascript File */
    info: File;

    /**  Base 64 URL encoded*/
    data: string;
}