
import { Log } from './log'
export class Timer {

    static timings: { [id: string]: number; } = {};

    static start(name: string): void {
        Timer.timings[name] = new Date().getTime();
    }

    static stop(name: string): number {
        return new Date().getTime() - Timer.timings[name]
    }

    /** 
     * return the lapsed time in MS as as string    
     */
    static stopAsString(name: string): string {
        return `'${name}' completed in ${Timer.stop(name)}  ms`;
    }

    /** 
  * prints lapsed time in MS with timestamp   
  */
    static stopAsLog(name: string) {
        Log.log(`'${name}' completed in ${Timer.stop(name)} ms`);
    }
}

