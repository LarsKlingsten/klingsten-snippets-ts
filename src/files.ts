export enum ReadAsFormat {
    binary = 0,
    base64 = 1,
    text = 2,
}
export enum Encoding {
    utf8 = 'utf-8',
    windows = 'windows-1252',
    mac = 'macintosh',
}

export interface IUploadFile {
    info: File;     /** javascript File */
    data: string;      /**  Base 64 URL encoded*/
}

export class Files {

    /** reads a file async, and returns string formatted base64 (default), text (as 'utf-8'), or binary 
     * 
     * @example
     * 
     * // typescript:
     * import { Files, ReadAsFormat} from 'klingsten-snippets'
     * 
     * async upload(filelist: FileList | null) { // single file
     *      if (!filelist || filelist.length !== 1) return;
     *      const strContent = await this.readFileAsync(filelist[0], ReadAsFormat.text);  
     *  }
     * <!-- HTML -->
     *   <input #file type="file" single (change)="upload(file.files)" title="add files"/>
     *                                 <!-- or -->
     *   <input #file type="file" multiple (change)="upload(file.files)" title="add files"/>
     */
    public static async readFileAsync(file: File, readAsFormat: ReadAsFormat = ReadAsFormat.base64, encoding = Encoding.utf8): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => {
                resolve(<string>reader.result);
            };
            reader.onerror = reject;

            if (readAsFormat == ReadAsFormat.text) {
                reader.readAsText(file, encoding);
            }
            else if (readAsFormat == ReadAsFormat.binary) {
                reader.readAsBinaryString(file);
            }
            else if (readAsFormat == ReadAsFormat.base64) {
                reader.readAsDataURL(file);
            } else {
                console.error("@readFileAsync invalid ReadAsFormat. Options are text, binary, base64 ")
            }

        });
    }

    /** uploads files to the browser, and return IUploadFile[]
     * 
     * usage
     * HTML: <input #file type="file" multiple (change)="upload(file.files)" />
     * 
     * Typescript:
     * files: UploadFile[] = File[] | FileList;
     * async upload(files:File[]) { this.files = await Files.uploadfiles(files) }  */
    public static async uploadFiles(filesInfo: FileList | File[]): Promise<IUploadFile[]> {
        const uploadFiles: IUploadFile[] = [];
        for (let i = 0; i < filesInfo.length; i++) {
            const file = filesInfo[i];
            const data = await Files.readFileAsync(file);
            uploadFiles.push({ data: data, info: file });
        }
        return uploadFiles;
    }

} 