import * as dayjs from 'dayjs'
import * as customParseFormat from 'dayjs/plugin/customParseFormat'

export class Dates {

    static isInit: boolean = Dates.init()

    private static init(): boolean {
        dayjs.extend(customParseFormat)
        return true;
    }


    /** Removes hours, minutes, seconds, and milliseconds 
     * @param new Date("Thu Mar 12 1970 23:27:12:112 GMT+2300")
     * @Return new Date("1970-03-12T00:00:00.000Z")  
     * */
    public static dateOnly(d: Date): Date {
        const correctedDate = new Date(0);
        const timeZoneOffset = d.getTimezoneOffset() * 60;
        const hoursOffset = d.getHours() * 60 * 60;
        const minutesOffset = d.getMinutes() * 60;
        const secondsOffset = d.getSeconds();
        const epochSeconds = d.getTime() / 1000 << 0;  // rounding
        correctedDate.setUTCSeconds(epochSeconds - timeZoneOffset - hoursOffset - minutesOffset - secondsOffset);

        return correctedDate;
    }

    /** epoch in seconds as integer*/
    public static unixTimeEpochSeconds(): number {
        return Date.now() / 1000 << 0;
    }

    public static getUtcTimeStamp(): Date {
        let d = new Date();
        return new Date(d.getTime() - d.getTimezoneOffset() * 60000); // 60 * 1000
    }

    // converts date to string in ISO 8601 format '19700101' 
    public static dateToYYYYMMDD(d: Date): string | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return Dates.dateToSqlString(d)!.replace(/-/g, '');
    }

    // converts epoch (number) to string in ISO 8601 format '1970-01-01T00:00:00.000Z' 
    public static epochToIsoString(epoch: number): string {
        return new Date(epoch).toISOString();
    }

    // converts epoch (number) to ISO 8601 format '1970-12-24 00:00:00' 
    public static epochToISODateTime(epoch: number): string {
        return new Date(epoch).toISOString().replace('T', ' ').substr(0, 19);
    }

    // converts epoch (number) to HH.MM:SS -time format '00:00:00' 
    public static epochToISOTime(epoch: number): string {
        return new Date(epoch).toISOString().substr(11, 8);
    }

    public static browserDateToUtc(d: Date): Date | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return new Date(d.getTime() - d.getTimezoneOffset() * 60000); // 60 * 1000
    }

    /** exactly as valueOf (but may or may not be easier to remember) */
    public static milliSecsSinceEpoch(d: Date): number | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return d.valueOf()
    }

    /** return date as epoch in milliseconds */
    public static getUtcEpoch(d: Date): number | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return Dates.dateToUtcTimeDate(d)!.getTime()
    }

    /** hours, minuttes, seconds are not included. Use custom format such as 'YYYY-MM-DD'.
     *  Uses dayjs' customParseFormat
     * @param dateStr: '2020-03-12'
     * @param dateFormat: 'YYYY-MM-DD'
     * @Return Utc Date (or undefined for errors)    */
    public static parseStrToDateUtc(dateStr: string, dateFormat: string): Date | undefined {
        const d = dayjs(dateStr, dateFormat, true)
        if (!d.isValid()) {
            return undefined;
        }
        return new Date(Date.UTC(d.year(), d.month(), d.date()));
    }

    /** hours, minuttes, seconds are not included. Use custom format such as 'YYYY-MM-DD'.
      *  Uses dayjs' customParseFormat
      * @param dateStr: '2020-03-12 14:15:16'
      * @param dateFormat: 'YYYY-MM-DD HH:mm:ss'
      * @Return Utc Date (or undefined for errors)    */
    public static parseStrToDateTimeUtc(dateStr: string, dateFormat: string): Date | undefined {
        const d = dayjs(dateStr, dateFormat, true)
        if (!d.isValid()) {
            return undefined;
        }
        return new Date(Date.UTC(d.year(), d.month(), d.date(), d.hour(), d.minute(), d.second(), d.millisecond()));
    }

    /** 
    * @param date '2022-01-26T00:00:00Z'
    * @Return Utc Date (or undefined for errors)    */
    public static parseStrToDateTimeUtcIso8601(dateStr: string): Date | undefined {
        const d = new Date(dateStr)
        if (isNaN(d.getTime())) {
            return undefined;
        }
        return d;
    }

    public static isValidDate(d: Date): boolean {
        return isNaN(d.getTime()) === false
    }

    /** Return UTC Date without hours, minuttes, seconds (or undefined) */
    public static removeHours(d: Date): Date | undefined {
        if (!Dates.isValidDate(d)) { return undefined; }
        return new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    }

    /** Return UTC Date incl hours, minuttes, seconds (or undefined) */
    public static dateToUtcTimeDate(d: Date): Date | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return new Date(Date.UTC(
            d.getFullYear(), d.getMonth(), d.getDate(),
            d.getHours(), d.getMinutes(), d.getSeconds(), d.getUTCMilliseconds()));
    }

    /**  return string is this format '1999-04-29 14:04:23' (or undefined) */
    public static dateToSqlString(d: Date): string | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return Dates.dateToISODateString(d)
    }

    /**  return string is this format '1999-04-29' (or undefined) */
    public static dateToISODateString(d: Date): string | undefined {
        if (!Dates.isValidDate(d)) { return undefined }
        return d.toISOString().substr(0, 10);
    }

    /** 
     * @deprecated
     * Use  parseStrToDateTimeUtcIso8601(), parseStrToDateTimeUtc or parseStrToDateUtc instead */
    public static StringToDate2(isoDateStr: string): Date | undefined {
        return Dates.dateToUtcDate(new Date(isoDateStr));
    }

    /** 
     * @deprecated
     * Use  removeHours instead */
    public static dateToUtcDate(d: Date): Date | undefined {
        if (!Dates.isValidDate(d)) { return undefined; }
        return Dates.removeHours(d);

    }

}