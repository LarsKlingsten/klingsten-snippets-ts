import { Compare } from '../compare'
import { Dates } from '../dates'

const testdateToSqlString = [{
    "name": "1a Dates.dateToISODateString",
    "func": Dates.dateToISODateString,
    "in": new Date("1970-03-12"),
    "expect": "1970-03-12" // javascript dates are f***** up
},
{
    "name": "2a Dates.dateToISODateString",
    "func": Dates.dateToISODateString,
    "in": new Date("1970-03-12"),
    "expect": "1970-03-12"
},
{
    "name": "3a Dates.dateToISODateString",
    "func": Dates.dateToISODateString,
    "in": new Date("1970-03-12T04:34"),
    "expect": "1970-03-12"
}, {
    "name": "4a Dates.dateToISODateString",
    "func": Dates.dateToISODateString,
    "in": new Date("1970-03-12T04:34"),
    "expect": "1970-03-12"
}]

const testdateToYYYYMMDD = [{
    "name": "1 Dates.dateToYYYYMMDD",
    "func": Dates.dateToYYYYMMDD,
    "in": new Date(1970, 2, 13),
    "expect": "19700312" // javascript dates are f***** up
},
{
    "name": "2 Dates.dateToSqlString",
    "func": Dates.dateToYYYYMMDD,
    "in": new Date("1970-03-12"),
    "expect": "19700312"
},
{
    "name": "3 Dates.dateToSqlString",
    "func": Dates.dateToYYYYMMDD,
    "in": new Date("1970-03-12T04:34"),
    "expect": "19700312"
}]

const testEpochToSqlDateTime = [{
    "name": "4 Dates.epochToSqlDateTime",
    "func": Dates.epochToISODateTime,
    "para1": 0,

    "expect": "1970-01-01 00:00:00"
}
]

const testEpochToIsoString = [{
    "name": "5 Dates.epochToIsoString",
    "func": Dates.epochToIsoString,
    "para1": 0,
    "expect": "1970-01-01T00:00:00.000Z"
}
]
const testEpochToSqlTime = [{
    "name": "6 Dates.epochToISOTime",
    "func": Dates.epochToISOTime,
    "para1": 0,
    "expect": "00:00:00"
}
]

const test_dateOnly = [{
    "name": "1 test_dateOnly",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 01:00:00 GMT+0100"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
},
{
    "name": "2 test_dateOnly",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 00:00:00 GMT+0100"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
}, {
    "name": "3 test_dateOnly",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 00:00:00 GMT+0000"),
    "expect": new Date("Thu Mar 12 1970 00:00:00 GMT+0000")
},
{
    "name": "4 test_dateOnly",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 00:00:00 GMT+0100"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
},
{
    "name": "5 test_dateOnly",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 23:00:00 GMT+2300"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
},
{
    "name": "6 test_dateOnly (minutes)",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 23:27:00 GMT+2300"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
}, {
    "name": "7 test_dateOnly (seconds)",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 23:27:12 GMT+2300"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
}, {
    "name": "8 test_dateOnly [other timezones])",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 23:27:12:112 GMT+2300"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
},
{
    "name": "9 test_dateOnly (milliseconds)",
    "func": Dates.dateOnly,
    "para1": new Date("Thu Mar 12 1970 23:27:12:112 GMT+0100"),
    "expect": new Date("Thu Mar 12 1970 01:00:00 GMT+0100")
}

]



interface iResult {
    testsCount: number
    successCount: number
    errorsCount: number;
}

export class TestDates {

    public static tests() {
        const r = TestDates.run_tests();
        console.log(TestDates, `tests=${r.testsCount} success=${r.successCount} errors=${r.errorsCount}`);
        console.log(TestDates, `note: not all tests are yielding 'counts'`);
        return r;
    }

    private static run_tests(): iResult {

        let testresults: iResult = {
            testsCount: 0,
            successCount: 0,
            errorsCount: 0,
        }

        test_dateOnly.forEach(e => {

            testresults.testsCount++
            let result = e.func(e.para1)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) {
                testresults.successCount++;
                //  console.log(Dates.dateToSqlString(result), result.toISOString());
            } else {
                console.log(`fails: ${e.name} values (${e.para1}) -> \ngot:   ${result} \nexpect:${e.expect}`);
                testresults.errorsCount++
            }

        });



        testdateToSqlString.forEach(e => {

            testresults.testsCount++
            let result = e.func(e.in)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) { testresults.successCount++; } else {
                console.log(`fails: ${e.name} values (${e.in}) -> got:${result} -> expect:${e.expect}`);
                testresults.errorsCount++
            }

        });



        testdateToYYYYMMDD.forEach(e => {
            let result = e.func(e.in)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) { testresults.successCount++; } else {
                console.log(`fails: ${e.name} values (${e.in}) -> got:${result} -> expect:${e.expect}`);
                testresults.errorsCount++
            }

        });

        testEpochToSqlDateTime.forEach(e => {
            let result = e.func(e.para1)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) { testresults.successCount++; } else {
                console.log(`fails: ${e.name} values (${e.para1}) -> got:${result} -> expect:${e.expect}`);
                testresults.errorsCount++
            }


        });

        testEpochToIsoString.forEach(e => {
            let result = e.func(e.para1)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) { testresults.successCount++; } else {
                console.log(`fails: ${e.name} values (${e.para1}) -> got:${result} -> expect:${e.expect}`);
                testresults.errorsCount++
            }

        });

        testEpochToSqlTime.forEach(e => {
            let result = e.func(e.para1)
            let isSuccess: boolean = Compare.compareObjAsStrings(result, e.expect);
            if (isSuccess) { testresults.successCount++; } else {
                console.log(`fails: ${e.name} values (${e.para1}) -> got:${result} -> expect:${e.expect}`);
                testresults.errorsCount++
            }

        });


        return testresults;
    }
}