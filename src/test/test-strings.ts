import { Compare } from '../compare';
import { Strings } from '../strings';


export class TestStrings {

    public static tests() {

        TestStrings.OldTest();
        TestStrings.test_removeNonPrintingCharsAndSpaces();
        TestStrings.test_splitExceptQuotes();
        TestStrings.test_replaceIllgalCharsEmail();
        TestStrings.test_splitNameFromEmail();
        TestStrings.test_isEmailValid();
    }

    private static test_isEmailValid() {
        const tests = [{
            name: "Strings.isEmailValid",
            func: Strings.isEmailValid,
            insAndOuts: [
                { in: 'lars@kln.net', exp: true },
                { in: 'lars@klen.net.sg', exp: true },
                { in: 'Lars', exp: false },
                { in: 'Lars@', exp: false },
                { in: 'Lars@@kl.net', exp: false },
                { in: 'Lars@k ld.net', exp: false },              
                { in: '@klinen.net.sg', exp: false },
                { in: 'lars@klten.', exp: false },
            ]
        }]

        tests.forEach(t => {
            let successCount = 0;
            for (let i = 0; i < t.insAndOuts.length; i++) {
                const r = t.insAndOuts[i];
                const result = Strings.isEmailValid(r.in)
                const errors = Compare.objects(result, r.exp, ['']);
                const isSuccess = errors.length === 0;
                if (isSuccess) {
                    successCount++;
                }
                else {
                    Compare.printErrors(errors, t.name + ' i=' + i)
                }
            }
            console.log("isSuccess", t.insAndOuts.length === successCount, `${t.name} tests=${t.insAndOuts.length} success=${successCount} `);
        })
    }

    private static test_replaceIllgalCharsEmail() {
        const tests = [{
            name: "Strings.replaceIllgalCharsEmail()",
            func: Strings.replaceIllgalCharsEmail,
            insAndOuts: [
                { in: 'Lars', exp: 'lars' },                     // to lower
                { in: '+', exp: '+' },                           // + is allowed
                { in: '!', exp: '!' },                           // ! is allowed
                { in: '<lars@eml.com>', exp: 'lars@eml.com' },   // remove < > 
                { in: ' lars@eml"".com ', exp: 'lars@eml.com' }, // remove "" 

            ]
        }]

        tests.forEach(t => {
            let successCount = 0;
            for (let i = 0; i < t.insAndOuts.length; i++) {
                const r = t.insAndOuts[i];
                const result = t.func(r.in)
                const errors = Compare.objects(result, r.exp, ['']);
                const isSuccess = errors.length === 0;
                if (isSuccess) {
                    successCount++;
                }
                else {
                    Compare.printErrors(errors, t.name + ' i=' + i)
                }
            }
            console.log("isSuccess", t.insAndOuts.length === successCount, `${t.name} tests=${t.insAndOuts.length} success=${successCount} `);
        })
    }


    private static test_splitNameFromEmail() {
        const test = {
            name: "Strings.splitNameFromEmail()",
            insAndOuts: [
                { in: 'Lars', exp: { name: 'Lars', email: '' } },
                { in: 'lars@EMAIL.com', exp: { name: '', email: 'lars@email.com' } },
                { in: 'lars k <lars@EMAIL.com>', exp: { name: 'lars k', email: 'lars@email.com' } },
                { in: '"lars k" <lars@EMAIL.com>', exp: { name: '"lars k"', email: 'lars@email.com' } },
                { in: "'lars, k' <lars@EMAIL.com>", exp: { name: "'lars, k'", email: 'lars@email.com' } },
                { in: '"lk@ckhansen.dk" <lk@ckhansen.dk>', exp: { name: '', email: 'lk@ckhansen.dk' } }
            ]
        }

        let successCount = 0;
        for (let i = 0; i < test.insAndOuts.length; i++) {
            const r = test.insAndOuts[i];
            const result = Strings.splitNameFromEmail(r.in)

            const errors = Compare.objects(result, r.exp, ['name', 'email']);
            const isSuccess = errors.length === 0;

            if (isSuccess) {
                successCount++;
            }
            else {
                Compare.printErrors(errors, test.name + ' i=' + i)
            }
        }
        console.log("isSuccess", test.insAndOuts.length === successCount, `${test.name} tests=${test.insAndOuts.length} success=${successCount} `);
    }


    private static OldTest() {

        const test_stringToNumber = [{
            "name": "1 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "120.123",
            "in_isUk": true,
            "expect": 120.123
        },
        {
            "name": "2 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "12,123",
            "in_isUk": false,
            "expect": 12.123
        },
        {
            "name": "3 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "12b,b123",
            "in_isUk": false,
            "expect": NaN
        },
        {
            "name": "4 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "12.23",  // should be a ',' but we can't disregard that it could be a "hundred" seperator
            "in_isUk": false,
            "expect": 1223
        },
        {
            "name": "5 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "-12.23",  // should be a '.' but we can't disregard that it could be a "hundred" seperator
            "in_isUk": false,
            "expect": -1223
        },
        {
            "name": "6 stringToNumber",
            "func": Strings.stringToNumber,
            "in_str": "-12.23",  // should be a ','
            "in_isUk": true,
            "expect": -12.23
        }
        ]

        const testStrings_StringToDate = [{
            "name": "1 Strings.StringToDate",
            "func": Strings.StringToDate,
            "para1": "1970-03-12T00:00:00",
            "expect": new Date(1970, 2, 12)
        }]

        const testStrings_PadStart = [
            {
                name: "1 Strings.PadStart",
                func: Strings.PadStart,
                str: "lars",
                len: 5,
                expect: ' lars'
            }, {
                name: "2 Strings.PadStart",
                func: Strings.PadStart,
                str: "lars",
                len: 5.5,
                expect: ' lars'
            }, {
                name: "3 Strings.PadStart",
                func: Strings.PadStart,
                str: "lars",
                len: 2,
                expect: 'lars'
            }, {
                name: "4 Strings.PadStart",
                func: Strings.PadStart,
                str: "lars",
                len: -2,
                expect: 'lars'
            }
        ]


        const testStrings_PadEnd = [
            {
                name: "1 Strings.PadEnd",
                func: Strings.PadEnd,
                str: "lars",
                len: 5,
                expect: 'lars '
            }, {
                name: "2 Strings.PadEnd",
                func: Strings.PadEnd,
                str: "lars",
                len: 5.5,
                expect: 'lars '
            }, {
                name: "1 Strings.PadEnd",
                func: Strings.PadEnd,
                str: "lars",
                len: 3,
                expect: 'lars'
            }, {
                name: "4 Strings.PadEnd",
                func: Strings.PadEnd,
                str: "lars",
                len: -2,
                expect: 'lars'
            }];


        const testSrings_uniqueID = [{
            "name": "1 testSrings_uniqueID",
            "func": Strings.uniqueID,

        }]
        testStrings_StringToDate.forEach(e => {
            let result = e.func(e.para1)
            let isSuccess = result.toString().localeCompare(e.expect.toString()) ? "failed" : "success";
            console.log(`${isSuccess} -> ${e.name} values (${e.para1}) -> ${result} -> expect: ${e.expect}`);

        });

        test_stringToNumber.forEach(e => {
            let result = e.func(e.in_str, e.in_isUk);
            let success = !result.toString().localeCompare(e.expect.toString()) ? "success" : "failed";
            console.log(`${success} -> ${e.name} values (${e.in_str}, ${e.in_isUk}) -> ${result} -> expect: ${e.expect}`);
        });

        test_stringToNumber.forEach(e => {
            let result = e.func(e.in_str, e.in_isUk);
            let success = !result.toString().localeCompare(e.expect.toString()) ? "success" : "failed";
            console.log(`${success} -> ${e.name} values (${e.in_str}, ${e.in_isUk}) -> ${result} -> expect: ${e.expect}`);
        });

        testStrings_PadStart.forEach(e => {
            let result = e.func(e.str, e.len);
            let success = !result.toString().localeCompare(e.expect.toString()) ? "success" : "failed";
            console.log(`${success} -> ${e.name} values (${e.str}, ${e.len}) -> '${result}' -> expect: '${e.expect}'`);
        });

        testStrings_PadEnd.forEach(e => {
            let result = e.func(e.str, e.len);
            let success = !result.toString().localeCompare(e.expect.toString()) ? "success" : "failed";
            console.log(`${success} -> ${e.name} values (${e.str}, ${e.len}) -> '${result}' -> expect: '${e.expect}'`);
        });


        // testGetCounter
        const myMap = new Map<string, boolean>();
        const iterations = 100000;
        for (let i = 0; i < iterations; i++) {
            testSrings_uniqueID.forEach(e => {
                let result = e.func()
                myMap.set(result, true);

            });
        }
        let success = myMap.size !== iterations ? "failed" : "success";
        console.log(`${success} -> ${testSrings_uniqueID[0].name} got ${myMap.size} unique ID -> expected: ${iterations}`);
    }

    private static test_removeNonPrintingCharsAndSpaces() {

        const tests = [{
            name: "Strings.removeNonPrintingCharsAndSpaces",
            func: Strings.removeNonPrintingCharsAndSpaces,
            insAndOuts: [
                { in: '', exp: '' },
                { in: 'ascii', exp: 'ascii' },
                { in: ' remove spaces ', exp: 'removespaces' },
                { in: 'check danish ææøåÆØ', exp: 'checkdanishææøåÆØ' },
                { in: 'new\nline', exp: 'newline' },
                { in: 'code@ckhansen.dk', exp: 'code@ckhansen.dk' },
                { in: 'lars <code@ckhansen.dk>', exp: 'lars<code@ckhansen.dk>' },
                { in: '"lars" <code@ckhansen.dk>', exp: '"lars"<code@ckhansen.dk>' },
                { in: "''", exp: "''" },
                { in: '""', exp: '""' },
                { in: "{[code]}", exp: "{[code]}" },
                { in: ",.;:$§!\#€%&/()=?", exp: ",.;:$§!\#€%&/()=?" },
                { in: "{[code]}", exp: "{[code]}" }
            ]

        }]

        tests.forEach(t => {
            let successCount = 0;
            for (let i = 0; i < t.insAndOuts.length; i++) {
                const r = t.insAndOuts[i];
                const result = t.func(r.in)
                const isSuccess = result === r.exp;
                if (isSuccess) {
                    successCount++;
                }
                else {
                    console.log(t.name, i, "isSuccess", isSuccess);
                    console.log(` ---> got='${result}' exp='${r.exp}'`);
                }
            }

            console.log("isSuccess", t.insAndOuts.length === successCount, `${t.name} tests=${t.insAndOuts.length} success=${successCount} `);
        })
    }

    private static test_splitExceptQuotes() {
        const tests = [{
            name: "splitByCommaSemicolon()",
            func: Strings.splitExceptQuotes,
            insAndOuts: [
                { in: 'Lars;K;', exp: ['Lars', 'K'] },
                { in: 'Lars; K', exp: ['Lars', 'K'] },
                { in: ' Lars, K ', exp: ['Lars', 'K'] },
                { in: 'Lars, K', exp: ['Lars', 'K'] },
                { in: '"Lars, K"', exp: ['"Lars, K"'] },
                { in: '"Lars; K"', exp: ['"Lars; K"'] },
                { in: '"Lars, K"', exp: ['"Lars, K"'] },
                { in: '"Lars, K" <lars@k.net>, , ,   ', exp: ['"Lars, K" <lars@k.net>'] },
                { in: '"Lars; K" <lars@k.net> ; , ;', exp: ['"Lars; K" <lars@k.net>'] },
                {
                    in: `"Lars, K1" <lars@k.net>, "lars, K2" <lars@klingsten.net>, lars K3 <lars@klingsten.net>`,
                    exp: ['"Lars, K1" <lars@k.net>', '"lars, K2" <lars@klingsten.net>', 'lars K3 <lars@klingsten.net>']
                },
                {
                    in: `'Lars, K1' <lars@k.net>, "lars, K2" <lars@klingsten.net>; lars K3 <lars@klingsten.net>`,
                    exp: [`'Lars, K1' <lars@k.net>`, '"lars, K2" <lars@klingsten.net>', 'lars K3 <lars@klingsten.net>']
                },


            ]

        }]

        tests.forEach(t => {
            let successCount = 0;
            for (let i = 0; i < t.insAndOuts.length; i++) {
                const r = t.insAndOuts[i];
                const result = t.func(r.in)
                const isSuccess = JSON.stringify(result) == JSON.stringify(r.exp);
                // console.log(t.name, i, "isSuccess", isSuccess);
                if (isSuccess) { successCount++; }
                else { console.log(' ---> got =', result, ' exp =', r.exp); }
            }
            console.log("isSuccess", t.insAndOuts.length === successCount, `${t.name} tests=${t.insAndOuts.length} success=${successCount} `);
        })
    }
}


