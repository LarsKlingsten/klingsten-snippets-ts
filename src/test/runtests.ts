import { TestDates } from './test-dates';
import { TestNumbers } from './test-numbers';
import { TestStrings } from './test-strings';
import { Log } from '../log'
import { TestCompare } from './test-compare';

Log.log("tests started");
TestDates.tests();
TestNumbers.tests();
TestStrings.tests();
TestCompare.tests();
Log.log("tests completed");
 
