import { Compare } from "../compare";

export class TestCompare {

    public static tests() {
        TestCompare.test_compareArrays();
        TestCompare.test_compareObjects();
    }

    private static test_compareArrays() {
        const array1 = [{ id: '1', name: 'peter' }, { id: '2', name: 'lars' }];
        const array2 = [{ id: 'a', name: 'peter' }, { id: 'b', name: 'lars' }];
        const compareTheseAttributesOnly = ['name']

        const errors = Compare.arrays(array1, array2, compareTheseAttributesOnly)
        Compare.printErrors(errors, 'Compare.compareArrays()');




    }

    private static test_compareObjects() {
        const obj1 = { id: '1', name: 'peter' };
        const obj2 = { id: 'a', name: 'peter' };
        const compareTheseAttributesOnly = ['name']

        const errors = Compare.objects(obj1, obj2, compareTheseAttributesOnly)
        Compare.printErrors(errors, 'Compare.compareObjects');

 


    }

}