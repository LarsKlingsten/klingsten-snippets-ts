import { Numbers } from '../numbers';

const testNumbersWithPrecision = [{
    "name": "1 Numbers.roundWithPrecision",
    "func": Numbers.roundWithPrecision,
    "para1": 120.12323,
    "para2": 3,
    "expect": 120.123
}]

const testGetCounter = [{
    "name": "1 Numbers.uniqueNumbers",
    "func": Numbers.getCounter,
    "para1": 120.12323,
    "para2": 3,
    "expect": 120.123
}]

export class TestNumbers {

    public static tests() {


        testNumbersWithPrecision.forEach(e => {
            let result = e.func(e.para1, e.para2)
            let success = result !== e.expect ? "failed" : "success";
            console.log(`${success} -> ${e.name} values (${e.para1}, ${e.para2}) -> ${result} -> expected: ${e.expect}`);
        });


        // testGetCounter
        const myMap = new Map<number, boolean>();
        const iterations = 10000;

        for (let i = 0; i < iterations; i++) {
            testGetCounter.forEach(e => {
                let result = e.func()
                myMap.set(result, true);
                // console.log(result);
            });
        }
        let success = myMap.size !== iterations ? "failed" : "success";
        console.log(`${success} -> ${testGetCounter[0].name} got ${myMap.size} unique numbers -> expected: ${iterations}`);


    }
}