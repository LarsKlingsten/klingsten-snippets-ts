

export interface CompareError {
    key: string,
    valA: any,
    valB: any
}

export class Compare {

    /** add errors to array */
    private static addToErrors(errors: CompareError[], key: string, valA: any, valB: any) {
        errors.push({ key: key, valA: valA, valB: valB });
    }

    /** comapore to objects using JSON.stringify  */
    public static compareObjAsStrings(a: any, b: any): boolean {
        return JSON.stringify(a) === JSON.stringify(b)
    }

    /** prints errors to console */
    public static printErrors(compareErrors: CompareError[], funcName: string) {
        console.log("isSuccess", compareErrors.length === 0, funcName);
        if (compareErrors) {
            compareErrors.forEach(error => {
                console.log(' --> ', error.key);
                console.log('     --> ', error.valA);
                console.log('     --> ', error.valB);
            });
        }
    }
    /** compares two arrays by attributes, and return an array of errors, if any, with 3 rows per error
     * @param  arrayA
     * @param  arrayB
     * @param  string[]  attributes of ArrayA/ArrayB, such a ['id','name']
     * @returns string[] of errors, if any
     *
     * example usage:
     * compare two arrays on attribute 'name', and disregard attribute 'id
     *
     * const array1 = [{ id: '1', name: 'peter'}, { id: '2', name: 'lars'}];
     * const array2 = [{ id: 'a', name: 'peter'}, { id: 'b', name: 'lars'}];
    *  const compareTheseAttributesOnly = ['name']
    *  const errors = Compare.Arrays(array1, array2, compareTheseAttributesOnly)
    *  printResult(errors, 'function name');
    *
    * // errors is an empty [], as 'name's are identical, even though the 'id's not
    */
    public static arrays(arrayA: any[], arrayB: any[], objectAttributes: string[]): CompareError[] {
        const errors: CompareError[] = [];
        if (!Array.isArray(arrayA) || !Array.isArray(arrayB)) {
            Compare.addToErrors(errors, 'Compare.arrays() cannot compare non array, try Compare.objects', arrayA, arrayB);
            return errors;
        }

        if ((arrayA.length !== arrayB.length)) {
            Compare.addToErrors(errors, 'Not Same Length', arrayA, arrayB);
            return errors;
        }
        // array loop
        for (let j = 0; j < arrayA.length; j++) {
            const objA = arrayA[j];
            const objB = arrayB[j];
            // objectAttributes loop
            for (let i = 0; i < objectAttributes.length; i++) {
                const key = objectAttributes[i];
                const valA = objA[key];
                const valB = objB[key];

                // no key provided, lets compare the string 
                if (key === "" && objA !== objB) {
                    Compare.addToErrors(errors, 'key="" (attr not provided)', objA, objB);
                }
                if (valA === undefined && valB === undefined) {
                    continue;
                }
                if (valA === undefined || valB === undefined) {
                    Compare.addToErrors(errors, key, valA, valB);
                    continue;
                }
                if (valA.toString() !== valB.toString()) {
                    Compare.addToErrors(errors, key, valA, valB);
                }
            }
            ;
        }
        return errors;
    }
    /** compares two objects attributes by toString() and
     * return an array of errors, if any, with 3 rows per error
     * @param  objA
     * @param  objB
     * @param  string[] of attributes of ArrayA/ArrayB, such as ['id','name']
     * @returns string[] of errors, if any
     *
     * example usage:
     * compare objects on attribute 'name', and disregard attribute 'id
     *
     * const obj1 = { id: '1', name: 'peter'};
     * const obj2 = { id: 'a', name: 'peter'};
     * const compareTheseAttributesOnly = ['name']
     * const errors = Compare.Objects(obj1, obj2, compareTheseAttributesOnly)
     * printResult(errors, 'function name');
    */
    public static objects(a: any, b: any, objectAttributes: string[]): CompareError[] {
        const errors: CompareError[] = [];

        if (Array.isArray(a) || Array.isArray(b)) {
            Compare.addToErrors(errors, 'Compare.objects cannot compare arrays, try Compare.arrays() instead', a, b);
            return errors;
        }

        for (let i = 0; i < objectAttributes.length; i++) {
            const key = objectAttributes[i];
            const valA = a[key];
            const valB = b[key];


            // no key provided, lets compare the string 
            if (key === "" && a !== b) {
                Compare.addToErrors(errors, 'key="" (attr not provided)', a, b);
            }
            if (valA === undefined && valB === undefined) {
                continue;
            }
            if (valA === undefined || valB === undefined) {
                Compare.addToErrors(errors, key, valA, valB);
                continue;
            }
            if (valA.toString() !== valB.toString()) {
                Compare.addToErrors(errors, key, valA, valB);
            }
        }
        ;
        return errors;
    }
}
