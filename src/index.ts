// export { Timer } from './timer';
export { Compare, CompareError } from './compare';
export { Dates } from './dates';
export { Files, ReadAsFormat, Encoding, IUploadFile } from './files';
export { Log } from './log';
export { Numbers } from './numbers';
export { Strings } from './strings';
export { Timer } from './timer';



