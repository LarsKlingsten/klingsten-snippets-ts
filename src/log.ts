import { Strings } from './strings';

export class Log {

    // logs takes one or more string arguments -> strings seperated by ',')
    // use UTC time
    static log(...msg: string[]): void {
        console.log(Log.formatDateToUTCString(new Date()) + " " + msg.join(' '));
    }

    static now(): string {
        return this.formatDate(new Date());
    }

    static formatDateToUTCString(d: Date): string {
        return `${Strings.PadStart(String(d.getUTCHours()), 2,'0')}:${Strings.PadStart(String(d.getUTCMinutes()), 2,'0')}:${Strings.PadStart(String(d.getUTCSeconds()), 2,'0')}.${Strings.PadEnd(String(d.getUTCMilliseconds()), 3,'0')}`;
    }
    static formatDate(d: Date): string {
        return `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}.${d.getMilliseconds()}`;
    }

}