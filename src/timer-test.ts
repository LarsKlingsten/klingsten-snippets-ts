import { Timer } from './timer';

export class TimerTest {

    static runTest() {
        Timer.start("test");
        console.log(Timer.stop("test"));
        console.log(Timer.stopAsString("test"));
        Timer.stopAsLog('test');
    }
}