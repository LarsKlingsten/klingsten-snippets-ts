
export class Numbers {

    /**  is the value is a number */
    public static isNumeric(value: any): boolean {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    /** Round up or down to nearest integer (not carefully tested) */
    public static round(n: number): number {
        if (n < 0) {
            return Math.ceil(n - 0.5)
        }
        return Math.floor(n + 0.5)
    }

    /**  Round up or down to nearest integer (not carefully tested) */
    public static roundWithPrecision(f: number, precision: number): number {
        let shift: number = Math.pow(10, precision)
        return Numbers.round(f * shift) / shift
    }


    private static counter = 0;
    /** get a counter running counter starting from 0, increasing by 1. Its unique for each client only 
     * For a unique ID for DB storage use instead Strings.uniqueID() */
    public static getCounter(): number {
        return Numbers.counter++
    }

}
