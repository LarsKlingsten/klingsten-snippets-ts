export class Strings {

    static regexUK = "^[-+]?(([0-9]+)((\.[0-9]+)?))$";
    static regexDK = "^[-+]?(([0-9]+)((\,[0-9]+)?))$";

    /** must be in iso date like this '2011-04-11T11:51:00' '2011-04-11T11:51:00Z'  */
    public static StringToDate(isoDateStr: string): Date {
        let d: Date = new Date(isoDateStr)
        return d
    }

    public static nowAsString(): string {
        var d = new Date();
        return `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}.${d.getMilliseconds()}`;
    }

    public static PadStart(str: string, targetLen: number, padStr: string = ' '): string {
        targetLen = targetLen >> 0; // makes its an integer
        if (str.length > targetLen) { return str; }
        targetLen = targetLen - str.length;

        let padding = '';
        for (let i = 0; i < targetLen; i++) {
            padding += padStr;
        }

        return padding + str;
    }
    public static PadEnd(str: string, targetLen: number, padStr: string = ' '): string {
        targetLen = targetLen >> 0; // makes its an integer
        if (str.length > targetLen) { return str; }
        targetLen = targetLen - str.length;
        let padding = '';
        for (let i = 0; i < targetLen; i++) {
            padding += padStr;
        }

        return str + padding;
    }


    /** strict convertion of a string to number, return NaN if string does not convert correctly
     *  input must contain only digits &&  "." (if UK style)
     * or "," (if Not UK Style)  &&  "-" (if at the start)  */
    // public static stringToNumber(s: string, isUkStyle: boolean): number {
    // if (s == undefined) { return NaN; }

    // let numberStr: string = s.trim();
    // if (isUkStyle) {
    //     if (!numberStr.match(Strings.regexUK)) {
    //         return NaN
    //     }
    //     numberStr = numberStr.replace(",", "");
    // }
    // else {
    //     if (!numberStr.match(Strings.regexDK)) {
    //         return NaN
    //     }
    //     numberStr = numberStr.replace(".", "");
    //     numberStr = numberStr.replace(",", ".");
    // }
    // return parseFloat(numberStr);

    public static stringToNumber(s: string, isUkFormat: boolean): number {
        if (s == undefined) { return NaN; }

        if (isUkFormat) {
            return Number(s)
        }
        return Number(s.replace(/\./g, '').replace(",", "."))
    }



    /** get unique ID, uses EPOCH plus random number  */
    public static uniqueID(): string {
        return Date.now() + Math.random().toString();
    }

    /** This uses the DOM Parser (available only in the the browser)
     * ref: https://stackoverflow.com/questions/5002111/how-to-strip-html-tags-from-string-in-javascript   */
    public static removeHtmlTags(html: string): string {
        let plain = new DOMParser().parseFromString(html, 'text/html').body.textContent || '';
        return plain.trim();
    }

    /** remove non printing chars, space, intended for use for string comparison with multilines  */
    public static removeNonPrintingCharsAndSpaces(s: string): string {
        return s.replace(/[^A-Za-zæøåÆØÅ0-9$§!"#€%&/\[\]\?{}()<>=@,.;':]/g, '');
    };



    /** splits a string by delimiters (',' or ',') except when delimiters are two quotes (' ' or " ")  */
    public static splitExceptQuotes(s: string, delimitor1: string = ';', delimitor2: string = ','): string[] {
        s += delimitor1;                // we add a delimitor at end , to avoid fixing a possible last item
        const resultArr: string[] = [];
        let doubleQuoteCount = 0;
        let singleQuoteCount = 0;
        let startPos = 0;
        for (let i = 0; i < s.length; i++) {

            // find "," or ";", and add to result array, except if within quotes (we just check if quotes counts are uneven)
            if ((s[i] === delimitor1 || s[i] === delimitor2) && doubleQuoteCount % 2 === 0 && singleQuoteCount % 2 === 0) {
                const subStr = s.substr(startPos, i - startPos).trim();  // get string, and remove empty spaces
                startPos = i + 1;                                        // skip current delimiter char
                if (subStr.length > 0) {                                 // avoid empty items
                    resultArr.push(subStr);
                }
            } else if (s[i] === `"`) {
                doubleQuoteCount++;
            } else if (s[i] === `'`) {
                singleQuoteCount++;
            }
        }
        return resultArr;
    }

    /** splits name and emails into {name:string, email:string} object.
     * email is lowercased, and illigal chars removed.
     * 
     * note: If the name is a email address  such as '"lars@kln.net" <lars@kln.net>', 
     * we return {name:'', email:'lars@kln.net'}, ie. the name is empty string
     * 
     * note: Uses Strings.replaceIllgalCharsEmail
     * @param string             'Leif Fenger <l@n.com>'
     * @returns {name, email }   {name:'leif , email: 'l@n.com>'} 
     */
    public static splitNameFromEmail(s: string): { name: string, email: string } {

        const arr = Strings.splitExceptQuotes(s, ' ', ' ')

        if (!arr) {
            return { name: '', email: '' };
        }

        const name = arr
            .filter(x => x.indexOf('@') === -1)
            .join(' ')
            .trim();

        // its possible that both the name and email part contains @
        // ex 'lars@kln.net' <'lars@kln.net'>'
        // to avoid this we take the last index (pop()) 
        const email = arr
            .filter(x => x.indexOf('@') > 0)  // get all items containing @
            .pop() || ''                      // get the last element email adr
                .trim()
                .toLocaleLowerCase();

        return { name: name, email: Strings.replaceIllgalCharsEmail(email) }
    }

    /** remove illegal chars in email adress (think non ASCII), and goes lowercase */
    public static replaceIllgalCharsEmail(s: string): string {
        const re = /([^a-zA-Z0-9@!#$%&'*+-\/=?^_`{|}~]+)/gm
        return s.replace(re, "").toLocaleLowerCase();
    }

    /** check if email address is valid  */
    public static isEmailValid(s: string): boolean {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return s.length !== s.replace(re, "").length;
    }

}